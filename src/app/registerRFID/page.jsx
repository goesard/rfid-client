import SideDrawer from "@/components/sideDrawer";
export default function registerRFID() {
  return (
    <>
      <div className="flex flex-wrap">
        <div className="z-10">
          <SideDrawer />
        </div>
        <div className="flex-1 w-screen">
          <div className="flex justify-center h-96 w-auto mt-5 items-start">
            <div className="card w-auto bg-base-300 shadow-xl z-0">
              <h2 className="card-title p-3">Register RFID</h2>

              <div className="flex p-3">
                <div className="flex-none w-24 h-auto items-center justify-center">
                  Item Code
                </div>
                <div className="flex-none ">
                  <input
                    type="text"
                    placeholder="Type here"
                    className="input input-bordered input-md w-full max-w-xs"
                  />
                </div>
              </div>
              <div className="flex p-3">
                <div className="flex-none w-24 h-auto  items-center justify-center">
                  Item ID
                </div>
                <div className="flex-none">
                  <input
                    type="text"
                    placeholder="Type here"
                    className="input input-bordered input-md w-full max-w-xs"
                  />
                </div>
              </div>
              <div className="flex p-3">
                <div className="flex-none w-24 h-auto  items-center justify-center">
                  Item Type
                </div>
                <div className="flex-none">
                  <input
                    type="text"
                    placeholder="Type here"
                    className="input input-bordered input-md w-full max-w-xs"
                    disabled
                  />
                </div>
              </div>
              <div className="flex p-3">
                <div className="flex-none w-24 h-auto  items-center justify-center">
                  Category
                </div>
                <div className="flex-none">
                  <input
                    type="text"
                    placeholder="Type here"
                    className="input input-bordered input-md w-full max-w-xs"
                    disabled
                  />
                </div>
              </div>
              <div className="flex p-3">
                <div className="flex-none w-24 h-auto  items-center justify-center">
                  Merk
                </div>
                <div className="flex-none">
                  <input
                    type="text"
                    placeholder="Type here"
                    className="input input-bordered input-md w-full max-w-xs"
                    disabled
                  />
                </div>
              </div>
              <div className="flex p-3">
                <div className="flex-none w-24 h-auto  items-center justify-center">
                  Manufacture
                </div>
                <div className="flex-none">
                  <input
                    type="text"
                    placeholder="Type here"
                    className="input input-bordered input-md w-full max-w-xs"
                    disabled
                  />
                </div>
              </div>
              <div className="flex p-3">
                <div className="flex-none w-24 h-auto  items-center justify-center">
                  PO
                </div>
                <div className="flex-none">
                  <input
                    type="text"
                    placeholder="Type here"
                    className="input input-bordered input-md w-full max-w-xs"
                  />
                </div>
                
              </div>
              <div className="flex p-3">
                <textarea
                  placeholder="Note"
                  className="textarea textarea-bordered textarea-md w-full max-w-xs"
                ></textarea>
              </div>
              <div className="card-actions p-2">
                <button className="btn btn-primary">Buy Now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
