import SideDrawer from "@/components/sideDrawer";
export default function listingRFID() {
  return (
    <>
    <div className="flex flex-wrap">
      <div>
        <SideDrawer />
      </div>
      <div className="flex-1 w-screen">
        <h1>
            Listing RFID
        </h1>
      </div>
    </div>
  </>
  );
}
