import Image from "next/image";
import SideDrawer from "@/components/sideDrawer";
import RFIDstats from "@/components/RFIDStats";

export default function Dashboard() {
  return (
    <>
      <div className="flex flex-wrap">
        <div>
          <SideDrawer />
        </div>
        <div className="flex-1 w-screen">
          <RFIDstats/>
        </div>
      </div>
    </>
  );
}
