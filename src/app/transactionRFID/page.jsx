"use client";
import SlimSelect from "slim-select";
import SideDrawer from "@/components/sideDrawer";
export default function transactionRFID() {
  return (
    <>
      <div className="flex flex-wrap">
        <div>
          <SideDrawer />
        </div>
        <div className="flex-1 w-screen">
          <h1>Transaction RFID</h1>
          <div className="w-72">
            <SlimSelect settings={{ disabled: true }}>
              <option value="all">All</option>
              <option value="1">Option 1</option>
              <option value="2">Option 2</option>
              <option value="3">Option 3</option>
            </SlimSelect>
          </div>
        </div>
      </div>
    </>
  );
}
