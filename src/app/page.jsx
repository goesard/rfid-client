"use client";
import Image from "next/image";
import Swal from "sweetalert2";
import btsLogo from "../assets/BTS_LOGO.jpeg";
import { useState } from "react";
import { useRouter } from "next/navigation";

export default function Home() {
  const Swal = require("sweetalert2");
  const router = useRouter();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = () => {
    console.log(username);
    console.log(password);
    if (username == "" || password == "") {
      console.log("Username or Password is required");
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.onmouseenter = Swal.stopTimer;
          toast.onmouseleave = Swal.resumeTimer;
        },
      });
      Toast.fire({
        icon: "error",
        title: "Username or Password incorrect",
      });
    } else {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.onmouseenter = Swal.stopTimer;
          toast.onmouseleave = Swal.resumeTimer;
        },
      });
      Toast.fire({
        icon: "success",
        title: "Signed in successfully",
      }).then(() => {
        router.push("/dashboard", { scroll: false });
      });
      console.log("Success");
    }
  };

  return (
    <div className="flex justify-center items-center h-screen w-full items-center">
      <div className="card card-compact w-96 bg-base-200 shadow-xl">
        <figure className="mt-5">
          <Image
            src={btsLogo}
            alt="BTS LOGO"
            width={300}
            height={500}
            // blurDataURL="data:..." automatically provided
            // placeholder="blur" // Optional blur-up while loading
          />
        </figure>
        <div className="card-body">
          <div className="card-body items-center text-center">
            <h3 className="card-title">Username</h3>
            <input
              type="text"
              placeholder="Username"
              className="input input-bordered input-md w-full max-w-xs"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <h1 className="card-title">Password</h1>
            <input
              type="password"
              placeholder="Password"
              className="input input-bordered input-md w-full max-w-xs"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <button
              className="btn btn-info w-full max-w-xs"
              onClick={handleLogin}
            >
              Login
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
